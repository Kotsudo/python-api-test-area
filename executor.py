import generator
import consumer
import csv
import os.path
from datetime import datetime

def api_sum_numbers_executor():
    result = get_base_row()
    a, b = generator.api_sum_numbers_generator()
    response = consumer.api_sum_numbers_consumer(a, b)

    result['a'] = a
    result['b'] = b
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'sum_numbers.csv')


def api_sum_array_executor():
    result = get_base_row()
    a = generator.api_sum_array_generator()
    response = consumer.api_sum_array_consumer(a)

    result['array_size'] = len(a)
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'sum_array.csv')


def api_linear_search_executor():
    result = get_base_row()
    a, x = generator.api_linear_search_generator()
    response = consumer.api_linear_search_consumer(a, x)

    result['array_size'] = len(a)
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'array_search.csv')


def api_prime_factors_executor():
    result = get_base_row()
    n = generator.api_prime_factors_generator()
    response = consumer.api_prime_factors_consumer(n)

    result['n'] = n
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'prime_factors.csv')


def api_bubble_sort_executor():
    result = get_base_row()
    a = generator.api_bubble_sort_generator()
    response = consumer.api_bubble_sort_generator(a)

    result['array_size'] = len(a)
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'bubble_sort.csv')


def api_graph_colouring_executor():
    result = get_base_row()
    matrix, m_colors, num_of_edges = generator.api_graph_colouring_generator()
    response = consumer.api_graph_colouring_consumer(matrix, m_colors)

    result['num_of_nodes'] = len(matrix)
    result['num_of_edges'] = num_of_edges
    result['colors'] = m_colors
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'graph_colouring.csv')


def api_graph_kruskal_executor():
    result = get_base_row()
    matrix, num_of_nodes, num_of_edges = generator.api_graph_kruskal_generator()
    response = consumer.api_graph_kruskal_consumer(matrix, num_of_nodes)

    result['num_of_nodes'] = num_of_nodes
    result['num_of_edges'] = num_of_edges
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'graph_kruskal.csv')


def api_graph_dijkstra_executor():
    result = get_base_row()
    matrix, start, num_of_edges = generator.api_graph_dijkstra_generator()
    response = consumer.api_graph_dijkstra_consumer(matrix, start)

    result['num_of_nodes'] = len(matrix)
    result['num_of_edges'] = num_of_edges
    result['start'] = start
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'graph_dijkstra.csv')


def api_hash_executor():
    result = get_base_row()
    password, hash_type = generator.api_hash_generator()
    response = consumer.api_hash_consumer(password, hash_type)

    result['hash'] = password
    result['hash_type'] = hash_type
    result['response_time'] = response['time']
    result['response_memory'] = response['memory']

    save_result(result, 'hash_cracker.csv')


def save_result(data, filename):
    filename = 'output/' + filename
    file_exists = os.path.isfile(filename)

    with open(filename, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n', fieldnames=data.keys())

        if not file_exists:
            writer.writeheader()  # file doesn't exist yet, write a header

        writer.writerow(data)

def get_base_row():
    now = datetime.now()

    result = {
        'date': now.strftime("%d/%m/%Y"),
        'time': now.strftime("%H:%M:%S"),
        'hour': now.strftime("%H"),
        'minute': now.strftime("%M"),
        'second': now.strftime("%S"),
        'week_day': now.weekday() + 1
    }

    return result