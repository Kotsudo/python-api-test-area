import executor

n = 1
while True:
    try:
        print('----------------------------------------')
        print('Run nr: ' + str(n))

        print('Executing sum numbers')
        executor.api_sum_numbers_executor()

        print('Executing sum array')
        executor.api_sum_array_executor()

        print('Executing linear search')
        executor.api_linear_search_executor()

        print('Executing prime factors')
        executor.api_prime_factors_executor()

        print('Executing bubble sort')
        executor.api_bubble_sort_executor()

        print('Executing graph colouring')
        executor.api_graph_colouring_executor()

        print('Executing graph kruskal algorithm')
        executor.api_graph_kruskal_executor()

        print('Executing graph dijkstra algorithm')
        executor.api_graph_dijkstra_executor()

        print('Executing hash cracking')
        executor.api_hash_executor()

        print('')

        n = n + 1
    except:
        print("Unexpected error")