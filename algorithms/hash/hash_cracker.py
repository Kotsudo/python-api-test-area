#!/usr/bin/python
from io import StringIO
import getopt
import hashlib
import sys
import os
import time

def info():
    a = 1


def checkOS():
    if os.name == "nt":
        operatingSystem = "Windows"
    elif os.name == "posix":
        operatingSystem = "posix"
    else:
        operatingSystem = "Unknown"
    return operatingSystem


class hashCracking:

    def saveToFile(self, hash, key):
        solved = False
        savedHashFile = open('algorithms/hash/SavedHashes.txt', 'a+')
        for solvedHash in savedHashFile:
            if hash in solvedHash.split(":")[1].strip():
                solved = True
        if solved is False:
            savedHashFile.write('%s:{}'.format(hash) % key)
            savedHashFile.write('\n')
        savedHashFile.close()

    def hashCrackWordlist(self, userHash, hashType, wordlist, verbose, bruteForce=False):
        self.lineCount = 0
        if "md5" in hashType:
            h = hashlib.md5
        elif "sha1" in hashType:
            h = hashlib.sha1
        elif "sha224" in hashType:
            h = hashlib.sha224
        elif "sha256" in hashType:
            h = hashlib.sha256
        elif "sha384" in hashType:
            h = hashlib.sha384
        elif "sha512" in hashType:
            h = hashlib.sha512
        else:
            return None

        if bruteForce is True:
            while True:
                line = "%s" % self.lineCount
                line.strip()
                numberHash = h(line).hexdigest().strip()
                if verbose is True:
                    sys.stdout.write('\r' + str(line) + ' ' * 20)
                    sys.stdout.flush()
                if numberHash.strip() == userHash.strip().lower():
                    self.saveToFile(numberHash, line)
                    return self.lineCount
                else:
                    self.lineCount = self.lineCount + 1
        else:
            with open(wordlist, "r") as infile:
                for line in infile:
                    line = line.strip()
                    lineHash = h(bytes(line, encoding="utf-8")).hexdigest()
                    if verbose is True:
                        sys.stdout.write('\r' + str(line) + ' ' * 20)
                        sys.stdout.flush()

                    if str(bytes(lineHash, encoding="utf-8")) == str(userHash.lower()):
                        self.saveToFile(lineHash, line)
                        return line
                    else:
                        self.lineCount = self.lineCount + 1

            return None

def main(argv):
    hashType = None
    userHash = None
    wordlist = None
    verbose = None
    numbersBruteForce = False
    try:
        opts, args = getopt.getopt(argv, "ih:t:w:nv", ["ifile=", "ofile="])
    except getopt.GetoptError:
        sys.exit(1)
    for opt, arg in opts:
        if opt == '-i':
            info()
            sys.exit()
        elif opt in ("-t", "--type"):
            hashType = arg.strip().lower()
        elif opt in ("-h", "--hash"):
            userHash = arg.strip().lower()
        elif opt in ("-w", "--wordlist"):
            wordlist = arg
        elif opt in ("-v", "--verbose"):
            verbose = True
        elif opt in ("-n", "--numbers"):
            numbersBruteForce = True
    if not (hashType and userHash):
        sys.exit()

    with open('algorithms/hash/SavedHashes.txt', 'a+') as savedHashFile:
        for solvedHash in savedHashFile:
            solvedHash = solvedHash.split(":")
            if userHash.lower() == solvedHash[1].strip():
                return solvedHash[0]

        else:
            try:
                h = hashCracking()
                return h.hashCrackWordlist(userHash, hashType, wordlist, verbose, bruteForce=numbersBruteForce)

            except IndexError:
                print('index error')
                return None

            except KeyboardInterrupt:
                print('keyboard interrupt')
                return None

            except IOError:
                print('IOError')
                return None

if __name__ == "__main__":
    main(sys.argv[1:])
