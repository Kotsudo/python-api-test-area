import requests
import generator
import time

BASE_URL = "http://localhost:5000/api"

def api_sum_numbers_consumer(a, b):
    data = {
        'a': str(a),
        'b': str(b)
    }

    return post_request(BASE_URL + "/1/sum-numbers", data)


def api_sum_array_consumer(a):
    data = {
        'a': str(a),
    }

    return post_request(BASE_URL + "/n/sum-array", data)


def api_linear_search_consumer(a, x):
    data = {
        'a': str(a),
        'x': str(x),
    }

    return post_request(BASE_URL + "/n/linear-search", data)


def api_prime_factors_consumer(n):
    data = {
        'n': str(n),
    }

    return post_request(BASE_URL + "/n-sqrt/prime-factors", data)


def api_bubble_sort_generator(a):
    data = {
        'a': str(a),
    }

    return post_request(BASE_URL + "/n-square/bubble-sort", data)


def api_graph_colouring_consumer(a, m):
    data = {
        'a': str(a),
        'm': str(m)
    }

    return post_request(BASE_URL + "/graph/colouring", data)


def api_graph_kruskal_consumer(a, n):
    data = {
        'a': str(a),
        'n': str(n)
    }

    return post_request(BASE_URL + "/graph/kruskal", data)


def api_graph_dijkstra_consumer(matrix, start):
    data = {
        'a': str(matrix),
        'start': str(start)
    }

    return post_request(BASE_URL + "/graph/dijkstra", data)


def api_hash_consumer(hash, hash_type):
    data = {
        'hash': str(hash),
        'type': str(hash_type)
    }

    return post_request(BASE_URL + "/hash-cracker", data)


def get_request(url, params):
    r = requests.get(url=url, params=params)
    return r.json()


def post_request(url, data):
    headers = {'content-type': 'application/x-www-form-urlencoded'}

    start_time = time.time()
    r = requests.post(url=url, data=data, headers=headers)

    return {
        'memory': r.json()['memory'],
        'time': time.time() - start_time
    }